# Student Helpdesk UI

Project is built with [Svelte](https://svelte.dev/) and its development kit [SvelteKit](https://kit.svelte.dev/).

## Prerequisite

- **Install [NodeJS](https://nodejs.org/en/) version 14 or above**

## Development

### How to run development environment
1. Install dependencies with `npm install` (or `pnpm install` or `yarn`)

2. Start a development server:

```bash
npm run dev

# or start the server and open the app in a new browser tab
npm run dev -- --open
```

### Quick tutorial
* [Basic tutorials of Svelte](https://svelte.dev/tutorial/basics)
* [SvelteKit docs](https://kit.svelte.dev/docs)
* Quick example:
  * Add a file with name `hello-world.svelte` in `src/routes` and add some `html` code.
  * SvelteKit will automatically create a route with name as filename (in this example which is `hello-world`) for new page.
  * Navigatge to `http://localhost:3000/hello-world` and see the result.

## Building

Before creating a production version of your app, install an [adapter](https://kit.svelte.dev/docs#adapters) for your target environment. Then:

```bash
npm run build
```

> You can preview the built app with `npm run preview`, regardless of whether you installed an adapter. This should _not_ be used to serve your app in production.
